class MessageBlock < Block

  def self.description
    'People'
  end

  def help
    'Permite comunicação com proteção do seu chaveiro GPG!'
  end

  def default_title
    'Chat GPG'
  end

  def profiles
    owner.people
  end
end

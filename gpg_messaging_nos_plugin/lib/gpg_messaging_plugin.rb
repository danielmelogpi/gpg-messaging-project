class GpgMessagingPlugin < Noosfero::Plugin

  def self.plugin_name
    "Gpg Guarded Messaging"
  end

  def self.plugin_description
    # FIXME
    "A plugin that does this and that, but not everything here."
  end

  def self.extra_blocks
    {
      PeopleBlock => {:type => Environment} #,
      #MembersBlock => {:type => Community},
      #FriendsBlock => {:type => Person}
    }
  end

end
